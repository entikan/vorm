module engine

import gg
import gx
import sokol.sapp
import sokol.gfx
import sokol.sgl


struct Task {}

struct TaskManager {
mut:
    tasks   []Task
}

struct GraphicsEngine {
mut:
	gg          &gg.Context = unsafe { nil }
	pipe        sgl.Pipeline
}

fn graphics_engine_loop(mut graphics_engine GraphicsEngine) {
	graphics_engine.gg.begin()
	//draws here
	graphics_engine.gg.end()
}

fn graphics_engine_start(mut graphics_engine GraphicsEngine) {
	gfx.setup(sapp.create_desc())
	sgl.setup(&sgl.Desc{max_vertices: 256 * 65536})

	mut pipdesc := gfx.PipelineDesc{}
	unsafe { vmemset(&pipdesc, 0, int(sizeof(pipdesc))) }
	pipdesc.colors[0] = gfx.ColorTargetState{
		blend: gfx.BlendState{
			enabled: true
			src_factor_rgb: .src_alpha
			dst_factor_rgb: .one_minus_src_alpha
		}
	}
	pipdesc.depth = gfx.DepthState{
		write_enabled: true
		compare: .less_equal
	}
	pipdesc.cull_mode = .back
	graphics_engine.pipe = sgl.make_pipeline(&pipdesc)
}

fn event_loop(mut ev gg.Event, mut graphics_engine GraphicsEngine) {
	//if ev.typ == .mouse_move {
	//	engine.mouse_x = int(engine.mouse_x)
	//	engine.mouse_y = int(engine.mouse_y)
	//}
	//parse events
}

pub fn graphics_engine_new() GraphicsEngine {
	mut graphics_engine := &GraphicsEngine{}
	graphics_engine.gg = gg.new_context(
		width: 800
		height: 600
		create_window: true
		window_title: 'Vorm 0.01'
		bg_color: gx.gray
		frame_fn: graphics_engine_loop
		init_fn: graphics_engine_start
		event_fn: event_loop
		user_data: graphics_engine
	)
	graphics_engine.gg.run()
	return *graphics_engine
}

