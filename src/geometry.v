module geometry

import gg.m4 { Mat4 }
import math.vec { Vec2, Vec3, Vec4 }


struct Transform {
mut:
	matrix []Mat4
}

struct NodePath {
mut:
	name 		string @[required]
	transform 	Transform
	children 	[]&NodePath
	parent 		&NodePath = unsafe {nil}
}

struct Camera {
mut:
	transform 	Transform
	near		f32 = 0.1
	far			f32 = 512
	fov			int = 90
}

struct Vertex {
mut:
	pos 	Vec3[f32]
	normal 	Vec3[f32]
	uv 		Vec2[f32]
	color 	Vec4[f32]
	blend 	int
}

fn vertex_flip(mut vertex Vertex) {
	vertex.normal *= Vec3[f32]{-1,-1,-1}
}

struct Polygon {
mut:
	vertices 	[]Vertex
	neighbors 	[]&Polygon
}

struct Mesh {
mut:
	polygons []Polygon
}

struct MeshNode {
mut:
	meshes []Mesh
}
